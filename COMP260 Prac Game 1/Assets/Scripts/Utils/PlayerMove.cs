﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {


    public GameObject player1, player2;
    //public Vector2 velocity; // in metres per second
    

    public float maxSpeed = 5.0f;
    public float acceleration = 1.0f; // in metres/second/second
    public float brake = 5.0f; // in metres/second/second
    public float turnSpeed = 30.0f; // in degrees/second
    private float speed = 0.0f; // in metres/second
    public float destroyRadius = 1.0f;
    private BeeSpaner beeSpawner;


    
    // Use this for initialization
    void Start () {
        beeSpawner = FindObjectOfType<BeeSpaner>();
    }
	
	// Update is called once per frame
	void Update () {


       
        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(transform.position, destroyRadius);
        }

        /*  
          // scale the velocity by the frame duration
          Vector2 move = velocity * Time.deltaTime;
          // move the object
          transform.Translate(move);
      */
        if (player2)
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                player2.transform.Translate(Vector3.up * maxSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                player2.transform.Translate(Vector3.down * maxSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                player2.transform.Translate(Vector3.left * maxSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                player2.transform.Translate(Vector3.right * maxSpeed * Time.deltaTime);
            }
        }
        /*
            // get the input values
            Vector2 direction;
            direction.x = Input.GetAxis("Horizontal");
            direction.y = Input.GetAxis("Vertical");
            // scale by the maxSpeed parameter
            Vector2 velocity = direction * maxSpeed;
            // move the object
            transform.Translate(velocity * Time.deltaTime);
        
       */

        // the horizontal axis controls the turn
        float turn = Input.GetAxis("Horizontal");

        // new turn speed

        float ntspeed = turnSpeed + speed;
        // turn the car
        transform.Rotate(0, 0, -turn * ntspeed  * Time.deltaTime);

        // the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis("Vertical");
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed > 0)
            {
                speed = speed - brake * Time.deltaTime;
            }
            else if (speed == 0)
            {
                speed = 0;
            }
            else{
                speed = speed + brake * Time.deltaTime;
            }
           
        }



        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);


        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;
        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.Self);

    }
}
