﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {



    public float beePeriod;
    public float minBeePeriod = 4;
    public float maxBeePeriod = 5;

    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;


     float speed = 4.0f;
     float turnSpeed = 180.0f; // degrees per second

    Transform target;
    private Vector2 heading = Vector2.right;
    public GameObject player1, player2;
    public ParticleSystem explosionPrefab;


    // Use this for initialization
    void Start () {
        // bee initially moves in random direction
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);
        // set speed and turnSpeed randomly
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,
        Random.value);

        player1 = GameObject.FindGameObjectWithTag("p1");
        player2 = GameObject.FindGameObjectWithTag("p2");


    }
	
	// Update is called once per frame
	void Update () {

        beePeriod = Random.RandomRange(minBeePeriod, maxBeePeriod);

        float p1dis = Vector3.Distance(transform.position, player1.transform.position);

        float p2dis = Vector3.Distance(transform.position, player2.transform.position);

        if (p1dis< p2dis)
        {
            target = player1.transform;
        }
        else if (p2dis < p1dis)
        {
            target = player2.transform;
        }
       
        // get the vector from the bee to the target
        Vector2 direction = target.position - transform.position;
        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;
        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }
        transform.Translate(heading * speed * Time.deltaTime);

    }

    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;

        Destroy(explosion.gameObject, explosion.duration);
    }
    void OnDrawGizmos()
    {
        // draw heading vector in redw
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);
        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        if (target!= null)
        {
            Vector2 direction = target.position - transform.position;
            Gizmos.DrawRay(transform.position, direction);
        }
        
       
    }
}
