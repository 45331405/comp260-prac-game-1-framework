﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpaner : MonoBehaviour {

    public float beePeriod;
    public float minBeePeriod, maxBeePeriod;
    public BeeMove beePrefab;
    public int nBees = 50;
    public Rect spawnRect;
    public BeeMove bref;
    // Use this for initialization
    void Start () {

        beePeriod = Random.Range(minBeePeriod, maxBeePeriod);
        bref = FindObjectOfType<BeeMove>();
        // instantiate a bee

        for (int i = 0; i < nBees; i++)
        {
            // instantiate a bee
            BeeMove bee = Instantiate(beePrefab);

            bee.transform.parent = transform;
            // give the bee a name and number
            bee.gameObject.name = "Bee " + i;

            // move the bee to a random position within
            // the spawn rectangle
            float x = spawnRect.xMin +
            Random.value * spawnRect.width;
            float y = spawnRect.yMin +
            Random.value * spawnRect.height;
            bee.transform.position = new Vector2(x, y);
        }

    }

    // Update is called once per frame
    void Update () {

        beePeriod -= Time.deltaTime;
        if (beePeriod <= 0)
        {
            // instantiate a bee
            BeeMove bee = Instantiate(beePrefab);

            bee.transform.parent = transform;
            // give the bee a name and number
            bee.gameObject.name = "Bee ";

            // move the bee to a random position within
            // the spawn rectangle
            float x = spawnRect.xMin +
            Random.value * spawnRect.width;
            float y = spawnRect.yMin +
            Random.value * spawnRect.height;
            bee.transform.position = new Vector2(x, y);
            beePeriod = Random.Range(minBeePeriod, maxBeePeriod);
        }
		
	}

    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
           
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }

        void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }
}
